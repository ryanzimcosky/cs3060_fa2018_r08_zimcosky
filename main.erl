#!/usr/bin/env escript
-module(main).
-export([main/1]).
%% Your code here!
%% Ryan Zimcosky
%% CS3060
%% Reflection 8
%% fall.erl

%% -  module(fall).
-  export([fallVelocity1/2]).
-  export([fallVelocity2/2]).
-  export([fallVelocity3/2]).
-  export([fallVelocity4/1]).
-  export([fallVelocity6/1]).



fallVelocity1(earth, Distance) -> 
    math:sqrt(2 * 9.8 * Distance);
fallVelocity1(moon, Distance) -> 
    math:sqrt(2 * 1.6 * Distance);
fallVelocity1(mars, Distance) -> 
    math:sqrt(2 * 3.71 * Distance).


fallVelocity2(Planet, Distance) -> 
    case Planet of
        earth -> math:sqrt(2 * 9.8 * Distance);
        moon -> math:sqrt(2 * 1.6 * Distance);
        mars -> math:sqrt(2 * 3.71 * Distance)
    end.




fallVelocity3(Planet, Distance) ->
    case Planet of
        earth -> Velocity = math:sqrt(2 * 9.8 * Distance);
        moon -> Velocity = math:sqrt(2 * 1.6 * Distance);
        mars -> Velocity = math:sqrt(2 * 3.71 * Distance)
    end,

    if
        Velocity == 0.0 -> io:fwrite("Stable");
        Velocity  < 5.0 -> io:fwrite("Slow");
        (Velocity >= 5.0) and (Velocity =< 10.0) -> io:fwrite("Moving");
        (Velocity >= 10.0) and (Velocity =< 20.0) -> io:fwrite("Fast");
        Velocity  > 20.0 -> io:fwrite("Speedy")
    end.

fallVelocity4({earth, Distance}) ->
    math:sqrt(2 * 9.8 * Distance);
fallVelocity4({moon, Distance}) ->
    math:sqrt(2 * 1.6 * Distance);
fallVelocity4({mars, Distance}) ->
    math:sqrt(2 * 3.71 * Distance).

fallVelocity6([{earth, Distance},{moon, Distance},{mars, Distance}]) ->
    Earth = math:sqrt(2 * 9.8 * Distance),
    Moon  = math:sqrt(2 * 1.6 * Distance),
    Mars  = math:sqrt(2 * 3.71 * Distance),
    [Earth, Moon, Mars].

main([Distance]) ->
    fallVelocity1(earth, Distance),
    fallVelocity1(moon, Distance),
    fallVelocity1(mars, Distance),
    
    fallVelocity2(earth, Distance),
    fallVelocity2(moon, Distance),
    fallVelocity2(mars, Distance),
    
    fallVelocity3(earth, Distance),
    fallVelocity3(moon, Distance),
    fallVelocity3(mars, Distance),
    
    fallVelocity4({earth, Distance}),
    fallVelocity4({moon, Distance}),
    fallVelocity4({mars, Distance}),
    
    fallVelocity6([{earth, Distance},{moon, Distance},{mars, Distance}]).
